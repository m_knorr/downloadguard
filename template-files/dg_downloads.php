<?php

/**
 * Direct redirect, we dont want to access this page directly from the browser
 */

if($input->urlSegment1 === 'hash') {
    $hash = $input->urlSegment2;
    echo $modules->get('DownloadGuard')->validateDownload($hash);
} else {
    throw new Wire404Exception();
}