# DownloadGuard for ProcessWire

## About DownloadGuard

This simple module was developed to give clients or users a unique download link to access file downloads you offer them.

## Installation & Usage

1. Place the module files in /site/modules/DownloadGuard/
2. In your admin, click Modules > Check for new modules
	3.	 Click "install" for __DownloadGuard__
4. In the module Settings you can edit the default count for max downloads and the time, the download should be active.
5. During the installation a Page „Downloads“ is created in your PageTree. You can create children under this Page to make Downloads. In the children page you get your unique hash and the ability to upload your file.

## Todo	

- Let the system create the download links, for easy copy and paste.